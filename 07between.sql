-- 查询积分大于等于1000或者积分小于等于3000之间的客户
SELECT *
FROM customers 
WHERE points >= 1000 AND points <= 3000;

-- 用between等价上面的写法
SELECT *
FROM customers 
WHERE points BETWEEN 1000 AND 3000;

-- 练习 找出1999到2000出生的用户
SELECT *
FROM customers
WHERE birth_date BETWEEN '1990-01-01' AND '2000-01-01';