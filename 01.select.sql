USE sql_store;

-- 返回客户的所有信息并且按第一个名字字母排序

SELECT * 
FROM customers
-- WHERE customer_id = 1
ORDER BY first_name;

-- 返回1,2两行数据

SELECT 1,2