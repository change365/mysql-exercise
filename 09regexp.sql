-- 搜索姓名中带有field的客户
SELECT *
FROM customers
WHERE last_name LIKE '%field%';


-- 用正则来实现上述功能
SELECT *
FROM customers
WHERE last_name REGEXP 'field';

-- 姓名以field开头的客户
SELECT *
FROM customers
WHERE last_name REGEXP '^field';

-- 姓名以field结尾的客户
SELECT *
FROM customers
WHERE last_name REGEXP 'field$';


-- 姓名带有field或mac或rose的客户
SELECT *
FROM customers
WHERE last_name REGEXP 'field|mac|rose';

-- 姓名带以field开头或带有mac或rose的客户
SELECT *
FROM customers
WHERE last_name REGEXP '^field|mac|rose';

-- 姓名带有 ge ie me的客户
SELECT *
FROM customers
WHERE last_name REGEXP '[gim]e';

-- 姓名带有 eg ei em的客户
SELECT *
FROM customers
WHERE last_name REGEXP 'e[gim]';

-- 姓名带有 e且e前面包含a到h的任一字母的客户
SELECT *
FROM customers
WHERE last_name REGEXP '[abcdefgh]e';
-- 等价上述写法
SELECT *
FROM customers
WHERE last_name REGEXP '[a-h]e';


