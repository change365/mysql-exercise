-- 默认排序是以id来排的,因为id是主键
SELECT *
FROM customers;

-- 以名称正序排序
SELECT *
FROM customers
ORDER BY first_name;

-- 以名称倒序排序
SELECT *
FROM customers
ORDER BY first_name DESC;

-- 以多列查询
-- 以名称和州名正序排序
SELECT *
FROM customers
ORDER BY first_name,state;

-- 以多列查询
-- 以名称和州名倒序排序
SELECT *
FROM customers
ORDER BY first_name DESC,state DESC;

-- 可以是不查询的列作为排序
SELECT first_name,last_name
FROM customers
ORDER BY birth_date;

-- 用别名进行排序
SELECT first_name,last_name,10 AS points
FROM customers
ORDER BY points,first_name;

-- 用选取列的位置进行排序
-- 下列的含义就是first_name和last_name
-- 尽量不要使用,如果选取的列就会改变排序顺序
SELECT first_name,last_name,10 AS points
FROM customers
ORDER BY 1,2;

-- 练习
SELECT order_id,product_id,quantity,unit_price,quantity*unit_price AS 'all price'
FROM order_items
WHERE order_id = 2
ORDER BY quantity*unit_price DESC;

-- 写法2
SELECT *,quantity*unit_price AS 'all price'
FROM order_items
WHERE order_id = 2
ORDER BY 'all price' DESC;
