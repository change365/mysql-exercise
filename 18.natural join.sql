-- 数据库会自动识别相同的字段并连接
-- 不推荐这样写
SELECT 
	o.order_id,
	c.first_name
FROM orders o
NATURAL JOIN customers c
