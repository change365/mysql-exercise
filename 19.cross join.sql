-- 交叉连接(笛卡尔积)
SELECT 
	c.first_name AS customer,
	p.`name` AS product
FROM customers c
CROSS JOIN products p
ORDER BY c.first_name;

-- 隐式交叉连接
SELECT 
	c.first_name AS customer,
	p.`name` AS product
FROM customers c,products p
ORDER BY c.first_name;

-- 练习 将shippers和products表进行显示连接和隐式连接
-- 写法1:隐式写法
SELECT *
FROM products p,shippers s
ORDER BY p.product_id;
-- 写法2:显示写法
SELECT *
FROM products p
CROSS JOIN shippers s
ORDER BY p.product_id