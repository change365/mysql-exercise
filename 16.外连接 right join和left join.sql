-- mysql有两种连接方式 外连接和内连接
-- 内连接只返回复合连接条件的数据
-- 外连接无论符不符合都会返回
-- 外连接有两种方式
-- 当是right join 返回所有右边的表
-- 当是left join 返回所有左边的表
USE sql_store;
-- 内连接
SELECT 
	c.customer_id,
	c.first_name,
	o.order_id
FROM customers c
JOIN orders o
	ON c.customer_id = o.customer_id
ORDER BY c.customer_id;

-- 外连接
SELECT 
	c.customer_id,
	c.first_name,
	o.order_id
FROM customers c
LEFT JOIN orders o
	ON c.customer_id = o.customer_id
ORDER BY c.customer_id;

-- 练习:合并产品表和订单产品表
SELECT p.product_id,p.`name`,oi.quantity
FROM products p
LEFT JOIN order_items oi
	ON p.product_id = oi.product_id;

-- 多表外连接 外连接客户表 订单表 物流表
-- 少用右连接,保持连接一致性
SELECT 
	c.customer_id,
	c.first_name,
	o.order_id,
	sh.`name` AS shipper
FROM customers c
LEFT JOIN orders o
	ON c.customer_id=o.customer_id
LEFT JOIN shippers sh
	ON o.shipper_id = sh.shipper_id
ORDER BY c.customer_id;


-- 练习: 订单表 order customers shippers order_statuses
SELECT 
	o.order_date,
	o.order_id,
	c.first_name,
	sh.`name` AS 'shipper',
	os.`name` AS 'status'
FROM orders o
JOIN customers c
	ON c.customer_id=o.customer_id
LEFT JOIN shippers sh
	ON o.shipper_id = sh.shipper_id
LEFT JOIN order_statuses os
	ON o.`status` = os.order_status_id
ORDER BY o.order_id;

-- 外连接同一张表
USE sql_hr;

SELECT 
	e.employee_id,
	e.first_name,
	m.first_name AS manager
FROM employees e
LEFT JOIN employees m
	ON e.reports_to = m.employee_id;
