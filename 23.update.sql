-- 更新一条数据
USE sql_invoicing;
SELECT * FROM sql_invoicing.invoices;

UPDATE invoices
SET payment_total = 10,
payment_date = '2019-03-01'
WHERE invoice_id = 1

-- 更新一条数据,使用默认值
UPDATE invoices
SET 
	payment_total = DEFAULT,
	payment_date = '2019-03-01'
WHERE invoice_id = 1;

-- 更新一条数据,插入对应的字段
USE sql_invoicing;
UPDATE invoices
SET 
	payment_total = invoice_total*0.5,
	payment_date = due_date
WHERE invoice_id = 3;

-- 更新多条数据
-- 只要条件满足多个就会自动更新
-- 更新全部把where去掉即可
UPDATE invoices
SET 
	payment_total = invoice_total*0.5,
	payment_date = due_date
WHERE client_id IN (3,4);

-- 练习
-- 给所有1990年以前的用户多加50积分
USE sql_store;
UPDATE customers
SET points = points+50
WHERE birth_date <'1990-01-01';

-- 通过子查询来更新数据
-- 在执行更新操作前,看一看子操作是什么
USE sql_invoicing;

UPDATE invoices
SET 
	payment_total = invoice_total*0.5,
	payment_date = due_date
WHERE client_id = (
	SELECT client_id
	FROM clients
	WHERE `name` = 'Myworks'
)

-- 有条件更新当前的表,直接加where
USE sql_invoicing;
UPDATE invoices
SET 
	payment_total = invoice_total*0.5,
	payment_date = due_date
WHERE payment_date IS NULL;

-- 练习:更新orders的积分大于3000的客户comments为金牌客户gold customers
USE sql_store;
UPDATE orders
SET 
	comments = 'Gold Customer'
WHERE customer_id IN 
(
	SELECT customer_id 
	FROM customers
	WHERE points >= 3000
)

