-- 查询客户在VA或FL或GA
SELECT *
FROM customers
WHERE state = 'VA' OR state = 'FL' OR state = 'GA';

-- 查询客户在VA或FL或GA 用IN的写法
SELECT *
FROM customers
WHERE state IN ('VA','FL','GA');

-- 查询客户不在VA或FL或GA 用IN的写法
SELECT *
FROM customers
WHERE state NOT IN ('VA','FL','GA');

-- 练习:查询products表中库存数是49,38,72
SELECT * 
FROM products
WHERE quantity_in_stock IN (49,38,72);