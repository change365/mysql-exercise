-- 使用on的连接条件会让sql看起来难懂
USE sql_store;

SELECT *
FROM order_items oi
JOIN order_item_notes oin
	ON oi.order_id = oin.order_id AND
		oi.product_id = oin.product_id;

-- 使用using子句代替上述操作
SELECT *
FROM order_items oi
JOIN order_item_notes oin
	USING (order_id,product_id);

-- 简化
SELECT 
	o.order_id,
	c.first_name,
	sh.`name` AS shipper
FROM orders o
JOIN customers c
	USING (customer_id)
LEFT JOIN shippers sh
	USING (shipper_id);

-- 练习:
USE sql_invoicing;

SELECT 
	p.date,
	c.`name`,
	p.amount,
	pm.`name`
FROM payments p
JOIN clients c
	USING (client_id)
JOIN payment_methods pm
	ON p.payment_method = pm.payment_method_id;