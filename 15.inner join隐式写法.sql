-- join连接表的隐式写法
-- 直接from多个表,在where中写连接条件

SELECT *
FROM orders o, customers c
WHERE o.customer_id = c.customer_id;

-- 如果没有写where的连接条件就是交叉表(笛卡尔积)
SELECT *
FROM orders o, customers c
