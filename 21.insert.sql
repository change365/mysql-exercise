-- 插入数据表
-- DEFAULT是让数据库填入默认值
USE sql_store;
INSERT INTO customers
VALUES (
	DEFAULT,
	'Lion',
	'Lee',
	'1999-01-01',
	NULL,
	'address',
	'city',
	'CA',
	DEFAULT
);

-- 插入方式2

INSERT INTO customers(
	first_name,
	last_name,
	birth_date,
	address,
	city,
	state
)
VALUES (
	'Lion2',
	'Lee',
	'1999-01-01',
	'address',
	'city',
	'CA'
);

-- 插入多条数据
INSERT INTO shippers (name)
VALUES ('Shipper1'),
				('Shipper2'),
				('Shipper3');

-- 练习:向产品表中添加3条产品记录
INSERT INTO products (
	`name`,
	quantity_in_stock,
	unit_price
)
VALUES ('drink',11,5.0),
				('noodles',5,10.0),
				('fruit',100,3.0);



-- 向关联表插入数据,
-- 添加订单记录以及订单中的产品
INSERT INTO orders(customer_id,order_date,`status`)
VALUES (1,'2019-01-02',1);
-- 最后插入成功数据的id
-- SELECT LAST_INSERT_ID(); 
INSERT INTO order_items
VALUES
	(LAST_INSERT_ID(),1,1,2.1),
	(LAST_INSERT_ID(),2,1,2.95)
