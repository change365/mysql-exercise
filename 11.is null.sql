-- 查询没有填写的电话的用户
SELECT *
FROM customers
WHERE phone IS NULL;

-- 查询填写的电话的用户
SELECT *
FROM customers
WHERE phone IS NOT NULL;

-- 练习:查询没有发货的订单
SELECT *
FROM orders
WHERE shipped_date IS NULL;