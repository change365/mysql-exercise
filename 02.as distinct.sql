-- 返回客户的 最后一个名字 第一个名字 积分  折扣
SELECT 
	last_name,
	first_name,
	points,
	points+10 AS discount_factor
FROM customers;

-- 返回所有客户的地址

SELECT state
FROM customers;

-- 返回所有客户的地址,并消除重复行


SELECT DISTINCT state
FROM customers
