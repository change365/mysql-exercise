-- 复制一个表
CREATE TABLE orders_archived AS
SELECT * FROM orders;
-- 用这种方法复制的表,没有主键和自动增加属性

-- 复制一个表的部分数据
INSERT INTO orders_archived
SELECT *
FROM orders
WHERE order_date < '2019-01-01';

-- 练习:
USE sql_invoicing;
CREATE TABLE invoices_archived AS
SELECT 
	i.invoice_id,
	i.number,
	c.`name`,
	i.invoice_total,
	i.payment_total,
	i.invoice_date,
	i.due_date,
	i.payment_date
FROM invoices i
JOIN clients c
	USING(client_id)
WHERE i.payment_date IS NOT NULL;