-- 查询 客户是九零后或者 积分大于1000并且家在VA
-- AND 的优先级 比 OR高
SELECT *
FROM customers
WHERE birth_date > '1990' OR 
		(points > 1000 AND state = 'VA');

-- 取反的操作符
SELECT *
FROM customers
WHERE NOT (birth_date > '1999-01-01' OR points > 1000 );

-- 取反等价的
SELECT *
FROM customers
WHERE birth_date <= '1999-01-01' AND points <= 1000;

-- 练习 查询order_items表中 订单id为6的产品总价大于30的记录
SELECT * 
FROM order_items
WHERE order_id = 6 AND quantity*unit_price > 30; 