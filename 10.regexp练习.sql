-- 练习1:找出first_name名字是elka或者ambur的
SELECT *
FROM customers
WHERE first_name REGEXP 'elka|ambur';

-- 练习2:检索last_name名字是以ey或者on结尾的
SELECT *
FROM customers
WHERE last_name REGEXP 'ey$|on$';

-- 练习3:检索last_name名字是以my开头或者包含se的客户
SELECT *
FROM customers
WHERE last_name REGEXP '^my|se';

-- 练习4:获取last_name名字包含b紧跟r或者u
SELECT *
FROM customers
WHERE last_name REGEXP 'b[ru]';
