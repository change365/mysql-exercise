-- UNION 增加表的行
-- 现在有两个查询
-- 查询1 有效订单
SELECT 
	order_id,
	order_date,
	'Active' AS 'status'
FROM orders
WHERE order_date >= '2019-01-01';

-- 查询2 归档订单
SELECT 
	order_id,
	order_date,
	'Archived' AS 'status'
FROM orders
WHERE order_date < '2019-01-01';

-- union合并两个查询到一张表上
SELECT 
	order_id,
	order_date,
	'Active' AS 'status'
FROM orders
WHERE order_date >= '2019-01-01'
UNION
SELECT 
	order_id,
	order_date,
	'Archived' AS 'status'
FROM orders
WHERE order_date < '2019-01-01';

-- 练习
SELECT 
	customer_id,
	first_name,
	points,
	'Gold' AS 'type'
FROM customers
WHERE points >=3000
UNION
SELECT 
	customer_id,
	first_name,
	points,
	'Sliver' AS 'type'
FROM customers
WHERE points >=2000 AND points < 3000
UNION
SELECT 
	customer_id,
	first_name,
	points,
	'Bronze' AS 'type'
FROM customers
WHERE points < 2000
ORDER BY first_name;