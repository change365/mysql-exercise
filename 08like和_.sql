-- 查询以姓名以b(不区分大小写)开头的客户
SELECT *
FROM customers
WHERE last_name LIKE 'b%';

-- 查询以姓名以y(不区分大小写)结尾的客户
SELECT *
FROM customers
WHERE last_name LIKE '%y';

-- 查询以姓名有6个字母,且最后是y的客户
SELECT *
FROM customers
WHERE last_name LIKE '_____y';

-- 练习 获取用户customers地址包含trail 或者 avenue 并电话号码是以9结尾的
SELECT *
FROM customers
WHERE (address LIKE '%trail%' OR address OR '%avenue%' )
      AND phone LIKE '%9';

-- 电话不是以9结尾的客户
SELECT *
FROM customers
WHERE phone NOT LIKE '%9';