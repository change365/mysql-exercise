-- 积分大于3000的所有客户信息
SELECT *
FROM customers
WHERE points > 3000;	

-- 地区在va的所有客户信息
SELECT *
FROM customers
WHERE state = 'va';

-- 地区不在va的客户的所有信息
SELECT *
FROM customers
WHERE state <> 'va';

-- 在1990-01-01年后的客户信息
SELECT *
FROM customers
WHERE birth_date > '1990-01-01';


-- test:获取今年所有的订单
SELECT *
FROM orders
WHERE order_date > '2019';

