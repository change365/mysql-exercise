-- 选择头3个用户
SELECT *
FROM customers
LIMIT 3;

-- 偏移量
-- 分页,一页有3个用户
-- page1 1-3 page2 4-6 page3 7-9
-- 返回page3就需要偏移6位,每页3个
SELECT *
FROM customers
LIMIT 6,3;

-- 找出前3个积分最高的用户
SELECT *
FROM customers
ORDER BY points DESC
LIMIT 3;

-- 注意子句的顺序,select ,from ,where, order by,limit  